import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practica_3/Home/bloc/escuchar_bloc.dart';
import 'package:practica_3/Home/home.dart';
import 'package:practica_3/Home/songs/bloc/favorites_bloc.dart';
import 'package:practica_3/LogIn/login.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'LogIn/bloc/loginauth_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: ".env");
  await Firebase.initializeApp();
  runApp(MultiBlocProvider(providers: [
    BlocProvider(
      create: (context) => LoginauthBloc()..add(VerifyAuthEvent()),
    ),
    BlocProvider(
      create: (context) => EscucharBloc(),
    ),
    BlocProvider(
      create: (context) => FavoritesBloc(),
    )
  ], child: Main()));
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primaryColorDark: Colors.indigo),
        darkTheme: ThemeData.dark(),
        home: BlocConsumer<LoginauthBloc, LoginauthState>(
            builder: ((context, state) {
          if (state is AuthSuccessState) {
            return HomePage();
          } else if (state is UnAuthState ||
              state is AuthErrorState ||
              state is SignOutSuccessState) {
            return LoginPage();
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        }), listener: (context, state) {
          const SnackBar(
            content: Text("Favor de autenticarse"),
          );
        }));
  }
}
