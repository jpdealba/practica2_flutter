part of 'loginauth_bloc.dart';

abstract class LoginauthState extends Equatable {
  const LoginauthState();

  @override
  List<Object> get props => [];
}

class LoginauthInitial extends LoginauthState {}

class AuthSuccessState extends LoginauthState {}

class UnAuthState extends LoginauthState {}

class SignOutSuccessState extends LoginauthState {}

class AuthErrorState extends LoginauthState {}

class AuthAwaitingState extends LoginauthState {}
