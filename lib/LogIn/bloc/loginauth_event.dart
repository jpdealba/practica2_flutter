part of 'loginauth_bloc.dart';

abstract class LoginauthEvent extends Equatable {
  const LoginauthEvent();

  @override
  List<Object> get props => [];
}

class VerifyAuthEvent extends LoginauthEvent {}

class AnonymousAuthEvent extends LoginauthEvent {}

class GoogleAuthEvent extends LoginauthEvent {}

class SignOutEvent extends LoginauthEvent {}
