import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutterfire_ui/firestore.dart';
import 'package:practica_3/Home/songs/song_element.dart';

class FavoriteSongs extends StatefulWidget {
  FavoriteSongs({Key? key}) : super(key: key);

  @override
  State<FavoriteSongs> createState() => _FavoriteSongsState();
}

class _FavoriteSongsState extends State<FavoriteSongs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: FirestoreListView(
          query: FirebaseFirestore.instance.collection("user_songs").where(
              "user_id",
              isEqualTo: FirebaseAuth.instance.currentUser!.uid),
          itemBuilder: (BuildContext context,
              QueryDocumentSnapshot<Map<String, dynamic>> document) {
            return SongElement(data: document.data(), id: document.id);
          },
        ));
  }
}
