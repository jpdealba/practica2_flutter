import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

part 'favorites_event.dart';
part 'favorites_state.dart';

class FavoritesBloc extends Bloc<FavoritesEvent, FavoritesState> {
  FavoritesBloc() : super(FavoritesInitial()) {
    on<OnAddFavorite>(add_favorite);
    on<OnDeleteFavorite>(delete_favorite);
  }

  FutureOr<void> add_favorite(event, emit) async {
    try {
      String song_header =
          "${event.song["artist"]} - ${event.song["album"]} - ${event.song["song_name"]}";
      var queryUser = await FirebaseFirestore.instance
          .collection("user_songs")
          .where("user_id", isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .where("header", isEqualTo: song_header);

      var docsRef = await queryUser.get();
      // Map<String, dynamic>? new_song_map = event.song;
      // new_song_map!["user_id"] = FirebaseAuth.instance.currentUser!.uid;
      // String song_header =
      //     "${event.song["artist"]} - ${event.song["album"]} - ${event.song["song_name"]}";
      // if (docsRef.data()?[song_header] == null) {
      //   Map<String, dynamic>? newMap = docsRef.data();
      //   newMap![song_header] = new_song_map;
      //   await queryUser.update(newMap);
      // }
      if (docsRef.docs.isEmpty) {
        Map<String, dynamic> upload = {
          "user_id": FirebaseAuth.instance.currentUser!.uid,
          "header": song_header
        };
        upload["song"] = event.song;
        await FirebaseFirestore.instance.collection("user_songs").add(upload);
      }

      emit(AddToFavorite());
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> delete_favorite(event, emit) async {
    try {
      String song_header =
          "${event.song["artist"]} - ${event.song["album"]} - ${event.song["song_name"]}";
      // query para traer el documento con el id del usuario autenticado
      var queryUser = await FirebaseFirestore.instance
          .collection("user_songs")
          .where("user_id", isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .where("header", isEqualTo: song_header);
      // query para sacar la data del documento
      var docsRef = await queryUser.get();

      if (docsRef.docs.isNotEmpty) {
        FirebaseFirestore.instance
            .collection("user_songs")
            .doc(docsRef.docs.first.id)
            .delete();
      }
      // if (docsRef.data()?[song_header] != null) {
      //   Map<String, dynamic>? newMap = docsRef.data();
      //   newMap?.remove(song_header);
      //   print(newMap);
      //   await queryUser.set(newMap!);
      // }

      emit(DeleteFromFavorite());
    } catch (e) {}
  }
}
